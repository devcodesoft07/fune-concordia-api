<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SectorImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'image'
    ];

    public function service(){
        return $this->belongsTo(Sector::class);
    }
}

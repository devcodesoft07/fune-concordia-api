<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'term',
        'lot_percentage',
        'lot_value',
        'fune_percentage',
        'fune_value',
        'initial_6',
        'initial_10',
        'initial_20',
        'cash',
    ];

    public function scopeGetDiscountByTermAndInv($query, $term, $invertion, $options){
        $query->where('term', $term);
        if($invertion>=$options->first && $invertion<$options->second){
            $query->select('initial_6 as initial_result');
        }elseif ($invertion>=$options->second && $invertion<$options->third){
            $query->select('initial_10 as initial_result');
        }elseif ($invertion>=$options->third){
            $query->select('initial_20 as initial_result');
        }
    }
}

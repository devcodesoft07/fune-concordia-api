<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'real_price',
        'discount_price',
        'predicted_price',
        'place_id',
    ];

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function images(){
        return $this->hasMany(SectorImage::class);
    }

    public function scopeGetSectorById($query, $id){
        return $query->join('places', 'sectors.place_id', 'places.id')
            ->where('sectors.id', $id)
            ->select('places.name as place', 'sectors.name', 'sectors.real_price','sectors.discount_price', 'sectors.predicted_price' );
    }
}

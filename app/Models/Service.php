<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'real_price',
        'discount_price',
        'predicted_price',
    ];

    public function images(){
        return $this->hasMany(ServiceImage::class);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'real_price' => (float)$this->real_price,
            'discount_price' => (float)$this->discount_price,
            'predicted_price' => (float)$this->predicted_price,
            'images' => ServiceImageResource::collection($this->images),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SectorImageCollection extends ResourceCollection
{
    public $collects = SectorImageResource::class;

    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
            'service_images' => [
                'list' => request()->url(),
            ],
        ];
    }
    public function with($request): array
    {
        return [
            'relationships' => [
                'sector' => request()->route('sector'),
            ],
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SectorCollection extends ResourceCollection
{
    public $collects = SectorResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'sectors' => [
                'list' => route('sectors.index'),
            ],
        ];
    }
}

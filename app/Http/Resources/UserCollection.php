<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    public $collects = UserResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'users' => [
                'list' => route('users.index'),
                'quantity_total' => $this->collection->count(),
                'quantity_active' => $this->collection->where('status','=','ACTIVE')->count(),
                'quantity_inactive' => $this->collection->where('status','=','INACTIVE')->count(),
            ],
        ];
    }
}

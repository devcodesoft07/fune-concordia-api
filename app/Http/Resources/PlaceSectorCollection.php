<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PlaceSectorCollection extends ResourceCollection
{
    public $collects = PlaceSectorResource::class;

    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
            'place_sectors' => [
                'list' => request()->url(),
            ],
        ];
    }
    public function with($request): array
    {
        return [
            'relationships' => [
                'place' => request()->route('place'),
            ],
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceOptionCollection extends ResourceCollection
{
    public $collects = ServiceOptionResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'initials' => [
                'list' => route('options.index'),
            ],
        ];
    }
}

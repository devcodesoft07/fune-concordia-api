<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaceSectorResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'real_price' => (string)$this->real_price,
            'discount_price' => (string)$this->discount_price,
            'predicted_price' => (string)$this->predicted_price,
            'images' => SectorImageResource::collection($this->images),
        ];
    }
}

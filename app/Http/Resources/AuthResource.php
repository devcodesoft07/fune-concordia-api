<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => (string)$this->name,
            'email' => (string)$this->email,
            'photo' => (string)$this->photo,
            'status' => (string)$this->status,
            'role_id' =>(int)$this->role_id,
            'role_name' =>(string)$this->role->name
        ];
    }
}

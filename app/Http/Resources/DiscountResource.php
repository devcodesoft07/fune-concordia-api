<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'=>(int)$this->id,
            'term'=> (int)$this->term,
            'lot_percentage'=> (float)$this->lot_percentage,
            'lot_value'=> (float)$this->lot_value,
            'fune_percentage'=> (float)$this->fune_percentage,
            'fune_value'=> (float)$this->fune_value,
            'initial_6'=> (float)$this->initial_6,
            'initial_10'=> (float)$this->initial_10,
            'initial_20'=> (float)$this->initial_20,
            'cash'=> (float)$this->cash,
        ];

    }
}

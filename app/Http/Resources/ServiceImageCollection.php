<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceImageCollection extends ResourceCollection
{
    public $collects = ServiceImageResource::class;

    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
            'service_images' => [
                'list' => request()->url(),
            ],
        ];
    }
    public function with($request): array
    {
        return [
            'relationships' => [
                'service' => request()->route('service'),
            ],
        ];
    }
}

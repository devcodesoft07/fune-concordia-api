<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'email' => (string)$this->email,
            'photo' => (string)$this->photo,
            'role_id' => (int)$this->role_id,
            'status' => (string)$this->status,
            'phone' => (string)$this->phone,
        ];
    }
}

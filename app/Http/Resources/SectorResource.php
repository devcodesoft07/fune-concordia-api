<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SectorResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'real_price' => (float)$this->real_price,
            'discount_price' => (float)$this->discount_price,
            'predicted_price' => (float)$this->predicted_price,
            'place_id' => (int)$this->place_id,
        ];
    }
}

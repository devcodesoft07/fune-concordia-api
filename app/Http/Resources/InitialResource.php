<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InitialResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'first' => (float)$this->first,
            'second' => (float)$this->second,
            'third' => (float)$this->third,
        ];
    }
}

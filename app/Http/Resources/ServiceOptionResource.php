<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'fune_closed' => (float)$this->fune_closed,
            'fune_cremated' => (float)$this->fune_cremated,
            'old_customer' => (float)$this->old_customer,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DiscountCollection extends ResourceCollection
{
    public $collects = DiscountResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'discounts' => [
                'list' => route('discounts.index'),
            ],
        ];
    }
}

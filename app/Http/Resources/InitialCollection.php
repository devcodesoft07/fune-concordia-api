<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InitialCollection extends ResourceCollection
{
    public $collects = InitialResource::class;

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'initials' => [
                'list' => route('initials.index'),
            ],
        ];
    }
}

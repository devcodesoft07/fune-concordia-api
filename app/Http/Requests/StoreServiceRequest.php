<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'real_price' => ['required', 'numeric', 'min:0'],
            'discount_price' => ['required', 'numeric', 'min:0'],
            'predicted_price' => ['required', 'numeric', 'min:0'],
            'image.*' => ['nullable', 'string', 'distinct', 'min:3']
        ];
    }
}

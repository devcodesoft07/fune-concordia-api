<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'. $this->user->id],
            'password' => ['nullable', 'min:8','max:255'],
            'photo' =>['nullable', 'string', 'max:255'],
            'role_id' => ['required', 'numeric', 'min:1', 'max:2'],
            'status' => ['required', 'in:ACTIVE,INACTIVE'],
        ];
    }
}

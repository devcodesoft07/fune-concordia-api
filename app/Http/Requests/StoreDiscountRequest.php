<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDiscountRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'term' => ['required', 'numeric', 'min:0'],
            'lot_percentage' => ['nullable', 'numeric', 'min:0', 'max:100'],
            'lot_value' =>  ['nullable', 'numeric', 'min:0'],
            'fune_percentage' =>  ['required', 'numeric', 'min:0', 'max:100'],
            'fune_value' =>  ['required', 'numeric', 'min:0'],
            'initial_6' => ['required', 'numeric', 'min:0', 'max:100'],
            'initial_10' => ['required', 'numeric', 'min:0', 'max:100'],
            'initial_20' => ['required', 'numeric', 'min:0', 'max:100'],
            'cash' => ['required', 'numeric', 'min:0', 'max:100'],
        ];
    }
}

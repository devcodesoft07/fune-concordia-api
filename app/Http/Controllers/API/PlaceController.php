<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePlaceRequest;
use App\Http\Requests\UpdatePlaceRequest;
use App\Http\Resources\PlaceCollection;
use App\Http\Resources\PlaceResource;
use App\Models\Place;
use Illuminate\Http\Response;

class PlaceController extends Controller
{
    public function index()
    {
        $this->authorize('view', Place::class);
        return new PlaceCollection(Place::paginate(20));
    }

    public function store(StorePlaceRequest $request)
    {
        $this->authorize('create', Place::class);
        $place = Place::create($request->all());
        return (new PlaceResource($place))
            ->additional(['message'=>'lugar creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Place $place)
    {
        $this->authorize('view', Place::class);
        return new PlaceResource($place);
    }

    public function update(UpdatePlaceRequest $request, Place $place)
    {
        $this->authorize('update', Place::class);
        $place->update($request->all());
        return (new PlaceResource($place))
            ->additional(['message' => 'lugar actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Place $place)
    {
        $this->authorize('delete', Place::class);
        $place->delete();
        return response()->json(['message'=>'lugar eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

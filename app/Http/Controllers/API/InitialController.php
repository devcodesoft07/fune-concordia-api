<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\InitialCollection;
use App\Http\Resources\InitialResource;
use App\Models\Initial;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InitialController extends Controller
{
    public function index()
    {
        return new InitialCollection(Initial::paginate(20));
    }

    public function store(Request $request)
    {
        $initial = Initial::create($request->all());
        return (new InitialResource($initial))
            ->additional(['message'=>'initial creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Initial $initial)
    {
        return new InitialResource($initial);
    }

    public function update(Request $request, Initial $initial)
    {
        $initial->update($request->all());
        return (new InitialResource($initial))
            ->additional(['message' => 'initial actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Initial $initial)
    {
        $initial->delete();
        return response()->json(['message'=>'initial eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use App\Models\Initial;
use App\Models\ServiceOption;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Imagick;
use PDF;
use Illuminate\Support\Facades\File;

class PlaceQuoteController extends Controller
{
    private $SERVICE = "Servicio Funerario";
    private $LOTE = "Lote Memorial";
    public function quotePlace(Request $request){
        $array = $request->all();
        $count_service = 0;
        $array_service = array();

        $count_lote = 0;
        $array_lote = array();

        foreach ($array as $item){
            $title = $item['title'];
            if ($title == $this->SERVICE){
                $count_service = $count_service + 1;
                $array_service[] = $item;
            }else {
                $count_lote = $count_lote + 1;
                $array_lote[] = $item;
            }
        }

        if ($count_service>0 && $count_lote == 0){
            $data = $this->quoteSample($count_service, $array_service);
        }elseif ($count_lote>0 && $count_service == 0){
            $data = $this->quoteSample($count_lote, $array_lote);
        }else if ($count_service>0 && $count_lote>0){
            $data = $this->quoteDouble($count_service, $array_service, $count_lote, $array_lote);
        }
        return $data;
    }

    public function quoteSample($quantity, $request){
        $data = $this->getDataQuote($quantity, $request);
        $user = User::getUser();
        $this->savePhoto($user->photo);
        $pdf = PDF::loadView('quotes.placeV2', ['data'=>$data, 'user'=>$user]);
        $pdf->save(public_path('quotePlace.pdf'));
        $this->convertPdfToImage();
        return response()->json([
            'message' => 'Imagen convertida de pdf a imagen',
            'data' => [
                'url'=>asset('quotePlace.jpg'),
            ]
        ], Response::HTTP_OK);
    }

    public function quoteDouble($count_service, $array_service, $count_lote, $array_lote){
        $service = $this->getDataQuote($count_service, $array_service);
        $lote = $this->getDataQuote($count_lote, $array_lote);
        $user = User::getUser();
        $pdf = PDF::loadView('quotes.placeDouble', ['service'=>$service, 'lote'=>$lote, 'user'=>$user]);
        $pdf->save(public_path('quotePlace.pdf'));
        $this->convertPdfToImage();
        return response()->json([
            'message' => 'Imagen convertida de pdf a imagen',
            'data' => [
                'url'=>asset('quotePlace.jpg'),
            ]
        ], Response::HTTP_OK);
    }

    public function getDataQuote($quantity, $request){
        $data = $request[0]['data'];
        $predicted_price = $data['predicted_price'];
        $invertion_initial = $request[0]['inv_initial'];
        $is_crematorium = $request[0]['crematorium_funeral'];
        $is_closed = $request[0]['closed_funeral'];
        $term = $request[0]['discount']['term'];
        $cash = $request[0]['discount']['cash'];
        $discount = $request[0]['discount'];
        $image = $this->getImageQuote($data['images'], $request[0]['title']);
        $discount_service = $this->getDiscountServiceOption($is_crematorium, $is_closed);
        $price_final = $predicted_price + $discount_service['fune_cremated'] - $discount_service['fune_closed'];

        $discount_invertion = $this->getDiscountInvIntial($price_final, $invertion_initial, $term);
        $discount_customer = $this->getDiscountCustomer($price_final, $request[0]['old_customer']);

        $price_final_total = $price_final - $discount_invertion['discount'] - $discount_customer['discount'];
        $method_payment = $this->getMethodPayment($price_final_total, $invertion_initial, $term, $discount, $request[0]['title']);
        $price_cash = $price_final - $price_final * ($cash/100);
        return [
            'image'=>$image,
            'title'=> $request[0]['title'],
            'name'=>$data['name'],
            'real_price' =>$data['real_price']*$quantity,
            'discount_price'=>$data['discount_price']*$quantity,
            'final_price'=>$price_final*$quantity,
            'discount_invertion'=>[
                'discount' => $discount_invertion['discount']*$quantity,
                'percentage' =>$discount_invertion['percentage']*$quantity,
            ],
            'discount_customer'=>[
                'discount' => $discount_customer['discount']*$quantity,
                'percentage' =>$discount_customer['percentage']*$quantity,
            ],
            'final_price_total'=>$price_final_total*$quantity,
            'cash_price'=>$price_cash*$quantity,
            'method_payment'=>[
                'invertion' => $method_payment['invertion']*$quantity,
                'balance_financed' => $method_payment['balance_financed']*$quantity,
                'term' => $method_payment['term']*$quantity,
                'monthly_fee' =>  $method_payment['monthly_fee']*$quantity,
            ],
            'observation'=>$request[0]['observation'],
            ];
    }

    public function convertPdfToImage()
    {
        $imagick = new Imagick();
        try {
            $imagick->setResolution(150,150);
            $imagick->readImage(asset('quotePlace.pdf'));
            $imagick->setImageFormat('jpeg');
            $imagick->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE );
            $imagick->writeImage('quotePlace.jpg');
            $imagick->clear();
            $imagick->destroy();
            echo "convertido pdf";
        } catch (\ImagickException $e) {
            echo 'no cargo el documento';
        }
    }

    public function getDiscountServiceOption($is_crematorium, $is_closed){
        $service_option = ServiceOption::first();
        return [
            'fune_cremated' => $is_crematorium ? $service_option->fune_cremated : 0,
            'fune_closed' => $is_closed ? $service_option->fune_closed : 0,
        ];
    }

    public function getDiscountInvIntial($price, $invertion, $term){
        $percentage_initial = ($invertion*100)/$price;
        $options = Initial::first();
        $discount = Discount::getDiscountByTermAndInv($term, $percentage_initial, $options)->first();
        if($discount->initial_result){
            return [
                'discount' => $price * ($discount->initial_result/100),
                'percentage' =>$discount->initial_result
            ];
        }else {
            return [
                'discount' => 0,
                'percentage' =>0,
            ];
        }
    }

    public function getDiscountCustomer($price, $old_customer){
        $service_options = ServiceOption::first();
        $discount_customer = 0;
        $discount_percentage = 0;
        if ($old_customer){
            $discount_customer = $price * ($service_options->old_customer/100);
            $discount_percentage = $service_options->old_customer;
        }
        return [
            'discount' => $discount_customer,
            'percentage' => $discount_percentage,
        ];
    }

    public function getMethodPayment($price, $invertion, $term, $discount, $title){
        $balance_financed = $price - $invertion;
        $factor = $this->getFactor($discount, $title);
        $term = $term * 12;
        if ($factor !=0 && $factor !=1){
            $monthly_fee = ceil($balance_financed*$factor);
        }else {
            $monthly_fee = ceil($balance_financed/$term);
        }
        return [
            'invertion' => $invertion,
            'balance_financed' => $balance_financed,
            'term' => $term,
            'monthly_fee' =>  $monthly_fee,
        ];
    }

    private function getFactor($discount, $title){
        if ($title == $this->SERVICE){
            $factor = $discount['fune_value'];
        }else {
            $factor = $discount['lot_value'];
        }
        return $factor;
    }

    private function getImageQuote($images, $name){
        $result = "";
        foreach ($images as $value) {
            if (strlen($value['image'])>10) {
                $result = $value['image'];
                $contents = file_get_contents($result);
                if ($name == $this->SERVICE){
                    $name = $this->SERVICE . '.jpg';
                }else {
                    $name = $this->LOTE . '.jpg';
                }
                File::put( public_path( 'quote/'.$name), $contents);
                break;
            }
        }
        return $result;
    }

    private function savePhoto($photo)
    {
        if(strlen($photo)>10){
            $contents = file_get_contents($photo);
            $name = "profile.jpg";
            File::put( public_path( 'quote/'.$name), $contents);
        }
    }
}

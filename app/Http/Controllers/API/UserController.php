<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index()
    {
        $this->authorize('view', User::class);
        return new UserCollection(User::sortByRol()->paginate(20));
    }

    public function store(StoreUserRequest $request)
    {
        $this->authorize('create', User::class);
        $user = User::create($request->all());
        return (new UserResource($user))
            ->additional(['message'=>'usuario creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(User $user)
    {
        $this->authorize('view', User::class);
        return new UserResource($user);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', User::class);
        $user->update($request->all());
        return (new UserResource($user))
            ->additional(['message' => 'usuario actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', User::class);
        $user->delete();
        return response()->json(['message'=>'usuario eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceOptionCollection;
use App\Http\Resources\ServiceOptionResource;
use App\Models\ServiceOption;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServiceOptionController extends Controller
{
    public function index()
    {
        return new ServiceOptionCollection(ServiceOption::paginate(20));
    }

    public function store(Request $request)
    {
        $option = ServiceOption::create($request->all());
        return (new ServiceOptionResource($option))
            ->additional(['message'=>'Service option creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ServiceOption $option)
    {
        return new ServiceOptionResource($option);
    }

    public function update(Request $request, ServiceOption $option)
    {
        $option->update($request->all());
        return (new ServiceOptionResource($option))
            ->additional(['message' => 'Service option actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ServiceOption $option)
    {
        $option->delete();
        return response()->json(['message'=>'Service option eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\AuthResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(StoreUserRequest $request){
        $user = User::create($request->toArray());
        $token = $user->createToken('Personal Access Token')->accessToken;
        return response()->json([
            'message' => 'El usuario se registro correctamente',
            'access_token' => $token
        ], Response::HTTP_OK);
    }
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();
                return response()->json([
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
                    'user' => new AuthResource($user),
                ]);
            } else {
                return response()->json([
                    'message'=>'Contraseña incorrecta',
                ], Response::HTTP_UNAUTHORIZED);
            }
        } else {
            return response()->json([
                'message'=>'Credenciales invalidas',
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function getUser(){
        $user = auth()->user();
        return response()->json([
            'message' => 'Información del usuario',
            'data' => $user,
        ], Response::HTTP_OK);
    }

    public function logout(){
        $token = auth()->user()->token();
        $token->revoke();
        return response()->json([
            'message' => 'El usuario ha cerrado de sesión',
        ], Response::HTTP_OK);
    }
}

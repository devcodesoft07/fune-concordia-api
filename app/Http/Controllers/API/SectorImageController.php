<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSectorImageRequest;
use App\Http\Requests\UpdateSectorImageRequest;
use App\Http\Resources\SectorImageCollection;
use App\Http\Resources\SectorImageResource;
use App\Models\Sector;
use App\Models\SectorImage;
use Illuminate\Http\Response;

class SectorImageController extends Controller
{
    public function index(Sector $sector)
    {
        $this->authorize('view', SectorImage::class);
        return new SectorImageCollection($sector->images()->paginate(20));
    }

    public function store(StoreSectorImageRequest $request, Sector $sector)
    {
        $this->authorize('create', SectorImage::class);
        $image = $sector->images()->create($request->all());
        return (new SectorImageResource($image))
            ->additional(['message'=>'imagen del sector creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Sector $sector, SectorImage  $image)
    {
        $this->authorize('view', SectorImage::class);
        return new SectorImageResource($image);
    }

    public function update(UpdateSectorImageRequest $request, Sector $sector, SectorImage $image)
    {
        $this->authorize('update', SectorImage::class);
        $image->update($request->all());
        return (new SectorImageResource($image))
            ->additional(['message' => 'imagen del sector actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Sector $sector, SectorImage $image)
    {
        $this->authorize('delete', SectorImage::class);
        $image->delete();
        return response()->json(['message'=>'imagen del sector eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreServiceImageRequest;
use App\Http\Requests\UpdateServiceImageRequest;
use App\Http\Resources\ServiceImageCollection;
use App\Http\Resources\ServiceImageResource;
use App\Models\Service;
use App\Models\ServiceImage;
use Illuminate\Http\Response;

class ServiceImageController extends Controller
{
    public function index(Service $service)
    {
        $this->authorize('view', ServiceImage::class);
        return new ServiceImageCollection($service->images()->paginate(20));
    }

    public function store(StoreServiceImageRequest $request, Service $service)
    {
        $this->authorize('create', ServiceImage::class);
        $image = $service->images()->create($request->all());
        return (new ServiceImageResource($image))
            ->additional(['message'=>'imagen del servicio creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Service $service, ServiceImage  $image)
    {
        $this->authorize('view', ServiceImage::class);
        return new ServiceImageResource($image);
    }

    public function update(UpdateServiceImageRequest $request, Service $service, ServiceImage $image)
    {
        $this->authorize('update', ServiceImage::class);
        $image->update($request->all());
        return (new ServiceImageResource($image))
            ->additional(['message' => 'imagen del servicio actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Service $service, ServiceImage $image)
    {
        $this->authorize('delete', ServiceImage::class);
        $image->delete();
        return response()->json(['message'=>'imagen del servicio eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

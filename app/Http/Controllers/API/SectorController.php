<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSectorRequest;
use App\Http\Requests\UpdateSectorRequest;
use App\Http\Resources\SectorCollection;
use App\Http\Resources\SectorResource;
use App\Models\Sector;
use Illuminate\Http\Response;

class SectorController extends Controller
{
    public function index()
    {
        $this->authorize('view', Sector::class);
        return new SectorCollection(Sector::paginate(20));
    }

    public function store(StoreSectorRequest $request)
    {
        $this->authorize('create', Sector::class);
        $sector = Sector::create($request->except('image'));
        $images = $request->get('image');
        foreach ($images as $image){
            $sector->images()->create(['image'=>$image]);
        }
        return (new SectorResource($sector))
            ->additional(['message'=>'sector creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Sector $sector)
    {
        $this->authorize('view', Sector::class);
        return new SectorResource($sector);
    }

    public function update(UpdateSectorRequest $request, Sector $sector)
    {
        $this->authorize('update', Sector::class);
        $sector->update($request->all());
        return (new SectorResource($sector))
            ->additional(['message' => 'sector actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Sector $sector)
    {
        $this->authorize('delete', Sector::class);
        $sector->delete();
        return response()->json(['message'=>'sector eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

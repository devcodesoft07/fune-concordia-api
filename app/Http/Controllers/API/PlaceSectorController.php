<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PlaceSectorCollection;
use App\Http\Resources\PlaceSectorResource;
use App\Models\Place;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlaceSectorController extends Controller
{
    public function index(Place $place)
    {
        $this->authorize('view', Place::class);
        return new PlaceSectorCollection($place->sectors()->orderBy('id','ASC')->paginate(20));
    }

    public function store(Request $request, Place $place)
    {
        $this->authorize('create', Place::class);
        $sector = $place->sectors()->create($request->all());
        return (new PlaceSectorResource($sector))
            ->additional(['message'=>'sector del lugar creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Place $place, Sector  $sector)
    {
        $this->authorize('view', Place::class);
        return new PlaceSectorResource($sector);
    }

    public function update(Request $request, Place $place, Sector $sector)
    {
        $this->authorize('update', Place::class);
        $sector->update($request->all());
        return (new PlaceSectorResource($sector))
            ->additional(['message' => 'sector del lugar actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Place $place, Sector $sector)
    {
        $this->authorize('delete', Place::class);
        $sector->delete();
        return response()->json(['message'=>'sector del lugar eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

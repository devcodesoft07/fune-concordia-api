<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDiscountRequest;
use App\Http\Requests\UpdateDiscountRequest;
use App\Http\Resources\DiscountCollection;
use App\Http\Resources\DiscountResource;
use App\Models\Discount;
use Illuminate\Http\Response;

class DiscountController extends Controller
{
    public function index()
    {
        $this->authorize('view', Discount::class);
        return new DiscountCollection(Discount::orderBy('term','ASC')->paginate(20));
    }

    public function store(StoreDiscountRequest $request)
    {
        $this->authorize('create', Discount::class);
        $discount = Discount::create($request->all());
        return (new DiscountResource($discount))
            ->additional(['message'=>'descuento creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Discount $discount)
    {
        $this->authorize('view', Discount::class);
        return new DiscountResource($discount);
    }

    public function update(UpdateDiscountRequest $request, Discount $discount)
    {
        $this->authorize('update', Discount::class);
        $discount->update($request->all());
        return (new DiscountResource($discount))
            ->additional(['message' => 'descuento actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Discount $discount)
    {
        $this->authorize('delete', Discount::class);
        $discount->delete();
        return response()->json(['message'=>'descuento eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\ServiceResource;
use App\Models\Service;
use Illuminate\Http\Response;

class ServiceController extends Controller
{
    public function index()
    {
        $this->authorize('view', Service::class);
        return new ServiceCollection(Service::orderBy('id','ASC')->paginate(20));
    }

    public function store(StoreServiceRequest $request)
    {
        $this->authorize('create', Service::class);
        $service = Service::create($request->except('image'));
        $images = $request->get('image');
        foreach ($images as $image){
            $service->images()->create(['image'=>$image]);
        }
        return (new ServiceResource($service))
            ->additional(['message'=>'servicio creado!'])
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Service $service)
    {
        $this->authorize('view', Service::class);
        return new ServiceResource($service);
    }

    public function update(UpdateServiceRequest $request, Service $service)
    {
        $this->authorize('update', Service::class);
        $service->update($request->all());
        return (new ServiceResource($service))
            ->additional(['message' => 'servicio actualizado!'])
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Service $service)
    {
        $this->authorize('delete', Service::class);
        $service->delete();
        return response()->json(['message'=>'servicio eliminado!'])
            ->setStatusCode(Response::HTTP_OK);
    }
}

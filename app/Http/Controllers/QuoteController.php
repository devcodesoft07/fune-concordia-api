<?php

namespace App\Http\Controllers;

use PDF;

class QuoteController extends Controller
{
    public function downloadPdf(){
        //return view('quotes.placev2');
        ob_end_clean();
        $pdf = PDF::loadView('quotes.placeV2');
        return $pdf->stream();
    }
}

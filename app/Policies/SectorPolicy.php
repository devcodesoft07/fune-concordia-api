<?php

namespace App\Policies;

use App\Models\Sector;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectorPolicy
{
    public function view(User $user)
    {
        return $user->hasPermission('view_sectors');
    }

    public function create(User $user)
    {
        return $user->hasPermission('create_sectors');
    }

    public function update(User $user)
    {
        return $user->hasPermission('update_sectors');
    }

    public function delete(User $user)
    {
        return $user->hasPermission('delete_sectors');
    }
}

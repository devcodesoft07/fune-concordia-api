<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiscountPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission('view_discounts');
    }

    public function create(User $user)
    {
        return $user->hasPermission('create_discounts');
    }

    public function update(User $user)
    {
        return $user->hasPermission('update_discounts');
    }

    public function delete(User $user)
    {
        return $user->hasPermission('delete_discounts');
    }
}

<?php

namespace App\Policies;

use App\Models\Place;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlacePolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission('view_places');
    }

    public function create(User $user)
    {
        return $user->hasPermission('create_places');
    }

    public function update(User $user)
    {
        return $user->hasPermission('update_places');
    }

    public function delete(User $user)
    {
        return $user->hasPermission('delete_places');
    }
}

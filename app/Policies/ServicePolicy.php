<?php

namespace App\Policies;

use App\Models\Service;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicePolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission('view_services');
    }

    public function create(User $user)
    {
        return $user->hasPermission('create_services');
    }

    public function update(User $user)
    {
        return $user->hasPermission('update_services');
    }

    public function delete(User $user)
    {
        return $user->hasPermission('delete_services');
    }
}

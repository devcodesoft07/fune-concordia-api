<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission('view_users');
    }

    public function create(User $user)
    {
        return $user->hasPermission('create_users');
    }

    public function update(User $user)
    {
        return $user->hasPermission('update_users');
    }

    public function delete(User $user)
    {
        return $user->hasPermission('delete_users');
    }
}

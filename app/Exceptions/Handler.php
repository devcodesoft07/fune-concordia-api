<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

        protected function invalidJson($request, ValidationException $exception)
        {
            return response()->json([
                'message' => __('Los datos proporcionados no son validos.'),
                'errors' => $exception->errors()
            ], $exception->status);
        }

    public function render($request, Throwable $e)
    {
        if ($e instanceof ModelNotFoundException){
            return response()->json([
                'message' => false,
                'error' => 'Error modelo no encontrado'
            ]);
        }else if ($e instanceof AuthorizationException){
            return response()->json([
                'message' => false,
                'error' => 'No autorizado.'
            ]);
        }
        return parent::render($request, $e);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'No Autenticado.'], Response::HTTP_UNAUTHORIZED);
        }

        //return redirect()->guest(route('login'));
    }
}

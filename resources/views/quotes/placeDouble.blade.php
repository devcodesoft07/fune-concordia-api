<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

        #info_header {
            text-align: center;
            margin-bottom: 20px;
        }
        #info_header img{
            height: 30px;
        }
        #content_images img {
            height: 220px;
            width: 150px;
            padding: 5px;
            border-radius: 5%;
            object-fit: cover;
        }
        #content_images .double-image {
            position: relative;
            top: 50px;
            width: 100%;
            margin-left: 190px;
            margin-bottom: 0;
            margin-top: 0;
        }
        #table-quote-double table {
            width:100%;
            margin:0 auto;
        }
        #info_quote table{
            width:100%;
            margin:0 auto;
            position:relative;
            border: 1px solid #2F312D;
            border-radius: 10px;
        }
        #info_quote table th {
            background: #F1F1EB;
            border: 1px solid rgba(115, 121, 110, 0.1);
            box-sizing: border-box;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 20px;
            color: #2F312D;
            opacity: 0.7;
            margin: 0 10px;
        }
        #info_quote table td {
            border: 0;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
        }
        #info_quote table .text-relevant{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 700;
            font-size: 12px;
            line-height: 20px;
            color: #2F312D;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table .text-opacity{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 16px;
            opacity: 0.5;
            color: #2F312D;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table .text-tall{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 700;
            font-size: 16px;
            line-height: 20px;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table td{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info_quote tr:nth-child(even){background-color: #F1F1EB}

        #method_payment table{
            max-width:800px;
            width:100%;
            margin:0 auto;
            position:relative;
            border-bottom: 1px solid #2F312D;
            border-left: 1px solid #2F312D;
            border-right: 1px solid #2F312D;;
            border-radius: 10px;
        }
        #method_payment th {
            background: #F1F1EB;
            border: 1px solid rgba(115, 121, 110, 0.1);
            box-sizing: border-box;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 20px;
            color: #2F312D;
            opacity: 0.7;
            margin: 0 10px;
        }
        #method_payment table td{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info-observation table{
            max-width:800px;
            width:100%;
            margin:0 auto;
            position:relative;
            border-bottom: 1px solid #2F312D;
            border-left: 1px solid #2F312D;
            border-right: 1px solid #2F312D;;
            border-radius: 10px;
        }
        #info-observation td {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info-contact {
            margin-top: 30px;
        }
        #info-contact table {
            max-width:800px;
            width:100%;
            margin:0 auto;
            border-bottom: 1px solid #2F312D;
            border-radius: 10px;
        }
        #info-contact td {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 24px;
            color: rgba(0, 0, 0, 0.87);
            padding-right: 20px;
        }
        #info-contact img {
            width: 80px;
            height: 80px;
            border-radius: 5%;
        }
        .text-number {
            margin-top: 0;
            color: #005300;
        }

        footer {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 12px;
            line-height: 16px;

            text-align: center;
            letter-spacing: 0.4px;

            color: #000000;

        }
    </style>
</head>
<body>
<section id="info_header">
    <img src="{{asset('images/logo_fune.png')}}" alt="40">
</section>

<section id="content_images">
    <div class="double-image">
        @if(strlen(@$service['image'])>10)
            <img src="{{asset('/quote/Servicio Funerario.jpg')}}" alt="40">
        @else
            <img src="{{asset('images/image.jpg')}}" alt="">
        @endif
        @if(strlen(@$lote['image'])>1)
            <img src="{{asset('/quote/Lote Memorial.jpg')}}" alt="40">
        @else
            <img src="{{asset('images/image.jpg')}}" alt="">
        @endif
    </div>
 </section>

<section id="table-quote-double">
    <table>
        <tr>
            <td>
                <section id="info_quote">
                    <table border="2">
                        <tr>
                            <th align="right">Producto:</th>
                            <td class="text-relevant">{{@$service['title']}}</td>
                            <td class="text-opacity">% Descuento</td>
                        </tr>
                        <tr>
                            <th align="right">Sector o Servicio:</th>
                            <td class="text-relevant">{{@$service['name']}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Precio Real:</th>
                            <td>{{@$service['real_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Descuento:</th>
                            <td>{{@$service['discount_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Precio Previsión:</th>
                            <td>{{@$service['predicted_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Descuento por Inicial y <br>Plazo:</th>
                            <td>{{@$service['discount_invertion']['discount']}} ($us)</td>
                            <td>- {{@$service['discount_invertion']['percentage']}} %</td>
                        </tr>
                        <tr>
                            <th align="right">Desc.Cliente/Combo:</th>
                            <td>{{@$service['discount_customer']['discount']}} ($us)</td>
                            <td>- {{@$service['discount_customer']['percentage']}} %</td>
                        </tr>
                        <tr>
                            <th align="right" class="text-tall"><b>PRECIO FINAL:</b></th>
                            <td>{{@$service['final_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right" class="text-tall"><b>CONTADO:</b></th>
                            <td>{{@$service['cash_price']}} ($us)</td>
                            <td></td>
                        </tr>
                    </table>
                </section>
            </td>
            <td>
                <section id="info_quote">
                    <table border="2">
                        <tr>
                            <th align="right">Producto:</th>
                            <td class="text-relevant">{{@$lote['title']}}</td>
                            <td class="text-opacity">% Descuento</td>
                        </tr>
                        <tr>
                            <th align="right">Sector o Servicio:</th>
                            <td class="text-relevant">{{@$lote['name']}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Precio Real:</th>
                            <td>{{@$lote['real_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Descuento:</th>
                            <td>{{@$lote['discount_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Precio Previsión:</th>
                            <td>{{@$lote['predicted_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right">Descuento por Inicial y <br>Plazo:</th>
                            <td>{{@$lote['discount_invertion']['discount']}} ($us)</td>
                            <td>- {{@$lote['discount_invertion']['percentage']}} %</td>
                        </tr>
                        <tr>
                            <th align="right">Desc.Cliente/Combo:</th>
                            <td>{{@$lote['discount_customer']['discount']}} ($us)</td>
                            <td>- {{@$lote['discount_customer']['percentage']}} %</td>
                        </tr>
                        <tr>
                            <th align="right" class="text-tall"><b>PRECIO FINAL:</b></th>
                            <td>{{@$lote['final_price']}} ($us)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th align="right" class="text-tall"><b>CONTADO:</b></th>
                            <td>{{@$lote['cash_price']}} ($us)</td>
                            <td></td>
                        </tr>
                    </table>
                </section>
            </td>
        </tr>
    </table>
</section>

<section id="method_payment">
    <table>
        <tr>
            <th align="center" colspan="4"><b>{{@$service['title']}}</b></th>
        </tr>
        <tr>
            <th align="left" class="text-opacity">Inversión <br> Inicial</th>
            <th align="left">Saldo a finalizar</th>
            <th align="left">Plazo</th>
            <th align="left">Cuota Mensual</th>
        </tr>
        <tr>
            <td>{{@$service['method_payment']['invertion']}} ($us)</td>
            <td>{{@$service['method_payment']['balance_financed']}} ($us)</td>
            <td>{{@$service['method_payment']['term']}}</td>
            <td>{{@$service['method_payment']['monthly_fee']}} ($us)</td>
        </tr>
    </table>
</section>

<section id="method_payment">
    <table>
        <tr>
            <th align="center" colspan="4"><b>{{@$lote['title']}}</b></th>
        </tr>
        <tr>
            <th align="left" class="text-opacity">Inversión <br> Inicial</th>
            <th align="left">Saldo a finalizar</th>
            <th align="left">Plazo</th>
            <th align="left">Cuota Mensual</th>
        </tr>
        <tr>
            <td>{{@$lote['method_payment']['invertion']}} ($us)</td>
            <td>{{@$lote['method_payment']['balance_financed']}} ($us)</td>
            <td>{{@$lote['method_payment']['term']}}</td>
            <td>{{@$lote['method_payment']['monthly_fee']}} ($us)</td>
        </tr>
    </table>
</section>

<section id="info-observation">
    <table>
        <tr>
            <td>
                <b>Observación {{@$service['title']}}:</b> {{@$service['observation']}} <br>
                <b>Observación {{@$lote['title']}}:</b> {{@$lote['observation']}} <br>
            </td>
        </tr>
    </table>
</section>
<section id="info-contact">
    <table>
        <tr>
            <td>
                @if(strlen(@$user->photo)>1)
                    <img src="{{asset('/quote/profile.jpg')}}" alt="">
                @else
                    <img src="{{asset('images/avatar.png')}}" alt="">
                @endif
            </td>
            <td>
                <div style="position:relative;">
                    <span style="position:absolute; left:-220px;">
                    {{@$user->name}}
                    </span>
                </div>
            </td>
            <td align="right">
                {{@$user->role->name}}
                <p class="text-number">{{@$user->phone}}</p>
            </td>
        </tr>
    </table>
</section>

<footer>
    <h4>Cochabamba -  {{ date('d M, Y') }}</h4>
</footer>
</body>
</html>

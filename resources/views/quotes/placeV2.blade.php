<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

        #info_header {
            text-align: center;
            margin-bottom: 20px;
        }
        #info_header img{
            height: 30px;
        }
        #content_images img {
            height: 300px;
            width: 220px;
            padding: 5px;
            border-radius: 5%;
            object-fit: cover;
            position: relative;
            left: 50%;
            transform: translate(-50%, 0);
            margin-bottom: 5px;
        }

        #info_quote table{
            max-width:800px;
            width:100%;
            margin:0 auto;
            position:relative;
            border: 1px solid #2F312D;
            border-radius: 10px;
        }
        #info_quote table th {
            background: #F1F1EB;
            border: 1px solid rgba(115, 121, 110, 0.1);
            box-sizing: border-box;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 20px;
            color: #2F312D;
            opacity: 0.7;
            margin: 0 10px;
        }
        #info_quote table td {
            border: 0;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
        }
        #info_quote table .text-relevant{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 700;
            font-size: 16px;
            line-height: 20px;
            color: #2F312D;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table .text-opacity{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 16px;
            opacity: 0.5;
            color: #2F312D;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table .text-tall{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 700;
            font-size: 17px;
            line-height: 20px;
            margin: 10px 0;
            padding: 2px 5px;
        }
        #info_quote table td{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info_quote tr:nth-child(even){background-color: #F1F1EB}

        #method_payment table{
            max-width:800px;
            width:100%;
            margin:0 auto;
            position:relative;
            border-bottom: 1px solid #2F312D;
            border-left: 1px solid #2F312D;
            border-right: 1px solid #2F312D;;
            border-radius: 10px;
        }
        #method_payment th {
            background: #F1F1EB;
            border: 1px solid rgba(115, 121, 110, 0.1);
            box-sizing: border-box;
            box-shadow: inset 0px -1px 0px rgba(255, 255, 255, 0.12);
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 20px;
            color: #2F312D;
            opacity: 0.7;
            margin: 0 10px;
        }
        #method_payment table td{
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info-observation table{
            max-width:800px;
            width:100%;
            margin:0 auto;
            position:relative;
            border-bottom: 1px solid #2F312D;
            border-left: 1px solid #2F312D;
            border-right: 1px solid #2F312D;;
            border-radius: 10px;
        }
        #info-observation td {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 20px;

            letter-spacing: 0.25px;
            color: #2F312D;
            margin: 10px 0px;
            padding: 2px 5px;
        }
        #info-contact {
            margin-top: 30px;
        }
        #info-contact table {
            max-width:800px;
            width:100%;
            margin:0 auto;
            border-bottom: 1px solid #2F312D;
            border-radius: 10px;
        }
        #info-contact td {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            color: rgba(0, 0, 0, 0.87);
            padding-right: 20px;
        }
        #info-contact img {
            width: 80px;
            height: 80px;
            border-radius: 5%;
        }
        .text-number {
            margin-top: 0;
            color: #005300;
        }

        footer {
            font-family: 'Arial';
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
            line-height: 16px;

            text-align: center;
            letter-spacing: 0.4px;

            color: #000000;
        }
    </style>
</head>
<body>
<section id="info_header">
    <img src="{{asset('images/logo_fune.png')}}" alt="40">
</section>

<section id="content_images">
    @if(strlen(@$data['image'])>10)
        @if(@$data['title'] == "Servicio Funerario")
            <img src="{{asset('/quote/Servicio Funerario.jpg')}}" alt="40">
        @else
            <img src="{{asset('/quote/Lote Memorial.jpg')}}" alt="40">
        @endif
    @else
        <img src="{{asset('images/image.jpg')}}" alt="">
    @endif
</section>

<section id="info_quote">
    <table border="2">
        <tr>
            <th align="right">Producto:</th>
            <td class="text-relevant">{{@$data['title']}}</td>
            <td class="text-opacity">% Descuento</td>
        </tr>
        <tr>
            <th align="right">Sector o Servicio:</th>
            <td class="text-relevant">{{@$data['name']}}</td>
            <td></td>
        </tr>
        <tr>
            <th align="right">Precio Real:</th>
            <td>{{@$data['real_price']}} ($us)</td>
            <td></td>
        </tr>
        <tr>
            <th align="right">Descuento:</th>
            <td>{{@$data['discount_price']}} ($us)</td>
            <td></td>
        </tr>
        <tr>
            <th align="right">Precio Previsión:</th>
            <td>{{@$data['final_price']}} ($us)</td>
            <td></td>
        </tr>
        <tr>
            <th align="right">Descuento por Inicial y <br>Plazo:</th>
            <td>{{@$data['discount_invertion']['discount']}} ($us)</td>
            <td>- {{@$data['discount_invertion']['percentage']}} %</td>
        </tr>
        <tr>
            <th align="right">Desc.Cliente/Combo:</th>
            <td>{{@$data['discount_customer']['discount']}} ($us)</td>
            <td>- {{@$data['discount_customer']['percentage']}} %</td>
        </tr>
        <tr>
            <th align="right" class="text-tall"><b>PRECIO FINAL:</b></th>
            <td>{{@$data['final_price_total']}} ($us)</td>
            <td></td>
        </tr>
        <tr>
            <th align="right" class="text-tall"><b>CONTADO:</b></th>
            <td>{{@$data['cash_price']}} ($us)</td>
            <td></td>
        </tr>
    </table>
</section>

<section id="method_payment">
    <table>
        <tr>
            <th align="left" class="text-opacity">Inversión <br> Inicial</th>
            <th align="left">Saldo a finalizar</th>
            <th align="left">Plazo</th>
            <th align="left">Cuota Mensual</th>
        </tr>
        <tr>
            <td>{{@$data['method_payment']['invertion']}} ($us)</td>
            <td>{{@$data['method_payment']['balance_financed']}} ($us)</td>
            <td>{{@$data['method_payment']['term']}}</td>
            <td>{{@$data['method_payment']['monthly_fee']}} ($us)</td>
        </tr>
    </table>
</section>

<section id="info-observation">
    <table>
        <tr>
            <td>
                Observación: {{@$data['observation']}}
            </td>
        </tr>
    </table>
</section>
<section id="info-contact">
    <table>
        <tr>
            <td>
                @if(strlen(@$user->photo)>10)
                    <img src="{{asset('/quote/profile.jpg')}}" alt="">
                @else
                    <img src="{{asset('images/avatar.png')}}" alt="">
                @endif
            </td>
            <td>
                <div style="position:relative;">
                    <span style="position:absolute; left:-200px;">
                    {{@$user->name}}
                    </span>
                </div>
            </td>
            <td align="right">
                {{@$user->role->name}}
                <p class="text-number">{{@$user->phone}}</p>
            </td>
        </tr>
    </table>
</section>

<footer>
    <h4>Cochabamba -  {{ date('d M, Y') }}</h4>
</footer>
</body>
</html>

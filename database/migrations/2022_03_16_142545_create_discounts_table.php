<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->integer('term');//plazo
            $table->float('lot_percentage')->nullable();
            $table->float('lot_value')->nullable();
            $table->float('fune_percentage');
            $table->float('fune_value');
            $table->float('initial_6');
            $table->float('initial_10');
            $table->float('initial_20');
            $table->float('cash');//contado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}

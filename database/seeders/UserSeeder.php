<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users= [
            [
                'name' => 'Daniel Guzman',
                'email' => 'admin@admin.com',
                'email_verified_at' => Null,
                'password' => 'password',
                'remember_token' => Null,
                'photo'=>NULL,
                'role_id' =>1,
                'phone' => '78569656',
            ],
            [
                'name' => 'Asesor',
                'email' => 'asesor@asesor.com',
                'email_verified_at' => Null,
                'password' => 'password', // password
                'remember_token' => Null,
                'photo'=>NULL,
                'role_id' =>2,
                'phone'=>NULL,
            ],
        ];
        foreach($users as $key => $value){
            User::create($value);
        }
    }
}

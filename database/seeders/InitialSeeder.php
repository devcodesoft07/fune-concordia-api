<?php

namespace Database\Seeders;

use App\Models\Initial;
use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $initials = [
            ['first' => 6,
             'second' => 10,
             'third' => 20,
            ]
        ];

        foreach ($initials as $item){
            Initial::create($item);
        }
    }
}

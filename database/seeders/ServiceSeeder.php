<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\ServiceImage;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    public function run()
    {
        $services = [
            [
                'name' => 'DIAMANTE PREMIUM',
                'real_price' => 4500,
                'discount_price' => 900,
                'predicted_price' => 3600,
                'image' => [null, null, null],
            ],
            [
                'name' => 'DIAMANTE',
                'real_price' => 2750,
                'discount_price' => 550,
                'predicted_price' => 2200,
                'image' => [null, null, null],
            ],
            [
                'name' => 'DOMICILIO DE LUJO',
                'real_price' => 2688,
                'discount_price' => 538,
                'predicted_price' => 2150,
                'image' => [null, null, null],
            ],
            [
                'name' => 'ESMERALDA',
                'real_price' => 2375,
                'discount_price' => 475,
                'predicted_price' => 1900,
                'image' => [null, null, null],
            ],
            [
                'name' => 'RUBI',
                'real_price' => 2125,
                'discount_price' => 425,
                'predicted_price' => 1700,
                'image' => [null, null, null],
            ],
            [
                'name' => 'PROVINCIA',
                'real_price' => 2125,
                'discount_price' => 425,
                'predicted_price' => 1700,
                'image' => [null, null, null],
            ],
            [
                'name' => 'DOMICILIO DE LUJO',
                'real_price' => 2125,
                'discount_price' => 425,
                'predicted_price' => 1700,
                'image' => [null, null, null],
            ],
        ];

        foreach ($services as $service) {
            $result = Service::create([
                'name' => $service['name'],
                'real_price' => $service['real_price'],
                'discount_price' => $service['discount_price'],
                'predicted_price' => $service['predicted_price'],
            ]);
            foreach ($service['image'] as $image){
                $result->images()->create(['image' => $image]);
            }
        }
    }
}

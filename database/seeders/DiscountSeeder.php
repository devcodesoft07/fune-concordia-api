<?php

namespace Database\Seeders;

use App\Models\Discount;
use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discounts = [
            [
                'term' => 1,
                'lot_percentage' => 0,
                'lot_value' => 0,
                'fune_percentage' => 0,
                'fune_value' => 0,
                'initial_6' => 4,
                'initial_10' => 6,
                'initial_20' => 8,
                'cash' => 10,
            ],
            [
                'term' => 2,
                'lot_percentage' => 0,
                'lot_value' => 0,
                'fune_percentage' => 0,
                'fune_value' => 0,
                'initial_6' => 3,
                'initial_10' => 5,
                'initial_20' => 7,
                'cash' => 10,
            ],
            [
                'term' => 3,
                'lot_percentage' => 0,
                'lot_value' => 0,
                'fune_percentage' => 0,
                'fune_value' => 0,
                'initial_6' => 3,
                'initial_10' => 5,
                'initial_20' => 7,
                'cash' => 10,
            ],
            [
                'term' => 4,
                'lot_percentage' => 2,
                'lot_value' => 0.02170,
                'fune_percentage' => 7,
                'fune_value' => 0.02395,
                'initial_6' => 2,
                'initial_10' => 4,
                'initial_20' => 6,
                'cash' => 10,
            ],
            [
                'term' => 5,
                'lot_percentage' => 4,
                'lot_value' => 0.01753,
                'fune_percentage' => 7,
                'fune_value' => 0.01980,
                'initial_6' => 0,
                'initial_10' => 2,
                'initial_20' => 4,
                'cash' => 10,
            ],
            [
                'term' => 6,
                'lot_percentage' => null,
                'lot_value' => null,
                'fune_percentage' => 8,
                'fune_value' => 0.01753,
                'initial_6' => 0,
                'initial_10' => 2,
                'initial_20' => 4,
                'cash' => 10,
            ],
            [
                'term' => 7,
                'lot_percentage' => null,
                'lot_value' => null,
                'fune_percentage' => 8,
                'fune_value' => 0.01559,
                'initial_6' => 0,
                'initial_10' => 2,
                'initial_20' => 4,
                'cash' => 10,
            ],
        ];
        foreach ($discounts as $discount) {
            Discount::create($discount);
        }
    }
}

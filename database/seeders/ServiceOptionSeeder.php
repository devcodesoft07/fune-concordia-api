<?php

namespace Database\Seeders;

use App\Models\ServiceOption;
use Illuminate\Database\Seeder;

class ServiceOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
             'fune_closed' => 150,
             'fune_cremated' => 500,
             'old_customer' => 2,
            ],
        ];

        foreach ($services as $item){
            ServiceOption::create($item);
        }
    }
}

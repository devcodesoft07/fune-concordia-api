<?php

namespace Database\Seeders;

use App\Models\Sector;
use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors = [
            //COLCAPIRHUA
            [
                'name' => 'CAPILLA',
                'real_price' => 9484,
                'discount_price' => 1836,
                'predicted_price' => 7648,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'PALMERAS',
                'real_price' => 9484,
                'discount_price' => 1836,
                'predicted_price' => 7648,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'PINOS ALTO',
                'real_price' => 8556,
                'discount_price' => 1656,
                'predicted_price' => 6900,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CEDROS',
                'real_price' => 8556,
                'discount_price' => 1656,
                'predicted_price' => 6900,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'ACACIAS',
                'real_price' => 7630,
                'discount_price' => 1477,
                'predicted_price' => 6153,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'PROLONGACION CAPILLA',
                'real_price' => 7630,
                'discount_price' => 1477,
                'predicted_price' => 6153,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'SAUCES',
                'real_price' => 7202,
                'discount_price' => 1394,
                'predicted_price' => 5808,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'OLIVOS',
                'real_price' => 7202,
                'discount_price' => 1394,
                'predicted_price' => 5808,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'PINOS',
                'real_price' => 7130,
                'discount_price' => 1380,
                'predicted_price' => 5750,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CEDROS NORTE',
                'real_price' => 7130,
                'discount_price' => 1380,
                'predicted_price' => 5750,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CEDROS MEDIO',
                'real_price' => 7130,
                'discount_price' => 1380,
                'predicted_price' => 5750,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'ROBLES',
                'real_price' => 7130,
                'discount_price' => 1380,
                'predicted_price' => 5750,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CIPRECES',
                'real_price' => 7130,
                'discount_price' => 1380,
                'predicted_price' => 5750,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CEIBOS ALTO',
                'real_price' => 6632,
                'discount_price' => 1284,
                'predicted_price' => 5348,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'EUCALIPTOS',
                'real_price' => 6346,
                'discount_price' => 1228,
                'predicted_price' => 5118,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'MOLLES',
                'real_price' => 6346,
                'discount_price' => 1228,
                'predicted_price' => 5118,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'CEIBOS',
                'real_price' => 6346,
                'discount_price' => 1228,
                'predicted_price' => 5118,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'EUCALIPTOS OESTE',
                'real_price' => 5919,
                'discount_price' => 1146,
                'predicted_price' => 4773,
                'image' => [null, null, null],
                'place_id' => 1,
            ],
            [
                'name' => 'MOLLES OESTE',
                'real_price' => 5919,
                'discount_price' => 1146,
                'predicted_price' => 4773,
                'image' => [null, null, null],
                'place_id' => 1,
            ],

            //TIQUIPAYA
            [
                'name' => 'DALIAS',
                'real_price' => 7192,
                'discount_price' => 1392,
                'predicted_price' => 5800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],
            [
                'name' => 'GIRASOLES',
                'real_price' => 7192,
                'discount_price' => 1392,
                'predicted_price' => 5800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'CAPILLA',
                'real_price' => 7192,
                'discount_price' => 1392,
                'predicted_price' => 5800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'GERANIOS',
                'real_price' => 6820,
                'discount_price' => 1320,
                'predicted_price' => 5500,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'DALIAS NORTE',
                'real_price' => 6572,
                'discount_price' => 1272,
                'predicted_price' => 5300,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'DALIAS OESTE',
                'real_price' => 5952,
                'discount_price' => 1152,
                'predicted_price' => 4800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'GIRASOLES ESTE',
                'real_price' => 5952,
                'discount_price' => 1152,
                'predicted_price' => 4800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'CAPILLA ESTE',
                'real_price' => 5952,
                'discount_price' => 1152,
                'predicted_price' => 4800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'GERANIOS ESTE',
                'real_price' => 5952,
                'discount_price' => 1152,
                'predicted_price' => 4800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'LIRIOS',
                'real_price' => 5704,
                'discount_price' => 1104,
                'predicted_price' => 4600,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'RETAMAS',
                'real_price' => 5704,
                'discount_price' => 1104,
                'predicted_price' => 4600,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'LIRIOS ESTE',
                'real_price' => 5456,
                'discount_price' => 1056,
                'predicted_price' => 4400,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'RETAMAS OESTE',
                'real_price' => 5456,
                'discount_price' => 1056,
                'predicted_price' => 4400,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'AZUCENAS',
                'real_price' => 5084,
                'discount_price' => 984,
                'predicted_price' => 4100,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'ORQUIDEAS',
                'real_price' => 5084,
                'discount_price' => 984,
                'predicted_price' => 4100,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'AZUCENAS ESTE',
                'real_price' => 4712,
                'discount_price' => 912,
                'predicted_price' => 3800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],[
                'name' => 'ORQUIDEAS OESTE',
                'real_price' => 4712,
                'discount_price' => 912,
                'predicted_price' => 3800,
                'image' => [null, null, null],
                'place_id' => 2,
            ],
        ];

        foreach ($sectors as $sector) {
            $result = Sector::create([
                'name' => $sector['name'],
                'real_price' => $sector['real_price'],
                'discount_price' => $sector['discount_price'],
                'predicted_price' => $sector['predicted_price'],
                'place_id' => $sector['place_id'],
            ]);
            foreach ($sector['image'] as $image){
                $result->images()->create(['image' => $image]);
            }
        }
    }
}

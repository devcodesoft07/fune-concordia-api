<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name','Administrador')->first();
        $admin->permissions()->attach([1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16, 17, 18,19,20]);

        $adviser = Role::where('name','Asesor')->first();
        $adviser->permissions()->attach([1, 5, 9, 13, 17]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'view_users'],
            ['name' => 'create_users'],
            ['name' => 'update_users'],
            ['name' => 'delete_users'],

            ['name' => 'view_discounts'],
            ['name' => 'create_discounts'],
            ['name' => 'update_discounts'],
            ['name' => 'delete_discounts'],

            ['name' => 'view_services'],
            ['name' => 'create_services'],
            ['name' => 'update_services'],
            ['name' => 'delete_services'],

            ['name' => 'view_places'],
            ['name' => 'create_places'],
            ['name' => 'update_places'],
            ['name' => 'delete_places'],

            ['name' => 'view_sectors'],
            ['name' => 'create_sectors'],
            ['name' => 'update_sectors'],
            ['name' => 'delete_sectors'],
        ];

        foreach ($permissions as $permission){
            Permission::create($permission);
        }
    }
}

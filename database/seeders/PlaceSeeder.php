<?php

namespace Database\Seeders;

use App\Models\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $places = [
            [
                'name' => 'Colcapirhua',
                'image' => null,
            ],
            [
                'name' => 'Tiquipaya',
                'image' => null,
            ],
        ];

        foreach ($places as $place){
            Place::create($place);
        }
    }
}

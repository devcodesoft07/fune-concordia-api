<?php

use App\Http\Controllers\API\InitialController;
use App\Http\Controllers\API\ServiceOptionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\DiscountController;
use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ServiceController;
use App\Http\Controllers\API\ServiceImageController;
use App\Http\Controllers\API\PlaceController;
use App\Http\Controllers\API\SectorController;
use App\Http\Controllers\API\SectorImageController;
use App\Http\Controllers\API\PlaceQuoteController;
use App\Http\Controllers\API\PlaceSectorController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
// public routes
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('test', [PlaceQuoteController::class, 'quotePlace']);

Route::group(['middleware' => ['auth:api']], function() {
    Route::get('user_profile', [AuthController::class, 'getUser']);
    Route::get('logout', [AuthController::class, 'logout']);
    Route::apiResource('discounts', DiscountController::class);
    Route::apiResource('users', UserController::class);
    Route::apiResource('services', ServiceController::class);
    Route::apiResource('services.images', ServiceImageController::class);
    Route::post('places/quote', [PlaceQuoteController::class, 'quotePlace']);
    Route::apiResource('places', PlaceController::class);
    Route::apiResource('sectors', SectorController::class);
    Route::apiResource('places.sectors', PlaceSectorController::class);
    Route::apiResource('sectors.images', SectorImageController::class);
    Route::apiResource('initials', InitialController::class);
    Route::apiResource('options', ServiceOptionController::class);

});
